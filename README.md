# MyNHS Relay

Securely transmits information about users of [mynhs](https://gitlab.com/nbnhs/mynhs) back to the site.

## Why?

The alternative is to store my credentials in the website itself. This is bad because:

1. anybody who can right-click and choose "inspect element" is now a master hacker and can very easily gain access to all member information
2. see #1
3. I do not need to provide any further reasons. #1 is more than enough.

This relay, on the other hand, does not expose secret/private/personal information in any way. It also uses HTTPS (faster than HTTP, anyway).

## How?

`src/index.ts` makes an HTTPS server that takes GET requests. The other two files handle Google's nonsense (`src/auth.ts` authenticates with Google's servers, `src/getCSV.ts` parses a downloaded CSV as JSON).

Essentially, the database used by this relay is Google Sheets. As you'll see in the deploy section of this guide, two sheet documents are necessary to get the relay up and running: roll (attendance) and service.

## Build Instructions

```bash
npm install
npx tsc
```

## Deploy

Access tokens are necessary to authenticate with Google, as are the file IDs of the sheets used by the program. Assuming you're using Heroku, modify the following JSON template and put it in an environment variable titled `SHEETS_ACCESS_CONFIG`:

```json
{
  "default": {
    "GOOGLE_CREDENTIALS": {
      "wrapper": {
        "installed": {
          "client_id": "",
          "project_id": "",
          "auth_uri": "",
          "token_uri": "",
          "auth_provider_x509_cert_url": "",
          "client_secret": "",
          "redirect_uris": [""]
        }
      },
      "token": {
        "access_token": "",
        "refresh_token": "",
        "scope": "https://www.googleapis.com/auth/drive.readonly",
        "token_type": "",
        "expiry_date": 0
      }
    },
    "ROLL_FILE_ID": "1",
    "ROLL_GID": "00001",
    "SERVICE_FILE_ID": "2",
    "SERVICE_GID": "00002"
  },
  "sci": {
    "ROLL_FILE_ID": "3",
    "ROLL_GID": "00016",
    "SERVICE_FILE_ID": "4",
    "SERVICE_GID": "00017"
  }
}
```

In the above template, the `default` object provides all the default rules, and the objects following it (in our case, `sci`) refer to subdomains sending requests to the relay. Where a subdomain rule is not specified, the `default` is used.

For example, if `sci.nbnhs.club` makes a request for service records to the relay, the rules in the `sci` object will be used. Since `GOOGLE_CREDENTIAL_METADATA` and `GOOGLE_TOKEN` are not set in the `sci` object, the `default` values will be used instead.

To deploy to Heroku from GitLab, set `HEROKU_API_KEY` equal to your API key in the CI/CD settings.
