import * as url from "url";
import { grepCSV, verifyUser } from "./functions/relayHelpers";
import getCSV from "./functions/getCSV";
import { IncomingMessage } from "http";
import getConfig from "./functions/getConfig";

/**
 * Processes a HTTP GET request and routes the information within to the relevant functions.
 * @param request The HTTP GET request to process
 * @returns The response to send to the client
 */
export default async function switchboard(
  request: IncomingMessage
): Promise<string> {
  console.log("New request received!");

  const parsed = url.parse(request.url, true).query;

  // must cast to string because parsed vals are either strings or string[]s
  const idNumber = parseInt(String(parsed.id));
  const subdomain = String(parsed.subdomain);

  console.log(`- From: ${idNumber} @ ${subdomain}`);
  console.log(`- Type: ${parsed.get}`);

  if (parsed.get === "roll") {
    console.log("- Downloading and interpreting CSV (roll)...");

    if (
      await verifyUser(
        idNumber,
        String(parsed.first),
        getConfig(subdomain, "ROLL_FILE_ID"),
        getConfig(subdomain, "ROLL_GID"),
        getConfig(subdomain, "GOOGLE_CREDENTIALS")
      )
    ) {
      return JSON.stringify(
        await grepCSV(
          getConfig(subdomain, "ROLL_FILE_ID"),
          getConfig(subdomain, "ROLL_GID"),
          idNumber,
          getConfig(subdomain, "GOOGLE_CREDENTIALS")
        )
      );
    }
    return "Invalid login";
  } else if (parsed.get === "service") {
    console.log("- Downloading and interpreting CSV (service)...");

    if (
      await verifyUser(
        idNumber,
        String(parsed.first),
        getConfig(subdomain, "ROLL_FILE_ID"),
        getConfig(subdomain, "ROLL_GID"),
        getConfig(subdomain, "GOOGLE_CREDENTIALS")
      )
    ) {
      return JSON.stringify(
        await grepCSV(
          getConfig(subdomain, "SERVICE_FILE_ID"),
          getConfig(subdomain, "SERVICE_GID"),
          idNumber,
          getConfig(subdomain, "GOOGLE_CREDENTIALS")
        )
      );
    }
    return "Invalid login";
  } else if (parsed.get === "eventNames") {
    console.log("- Downloading and interpreting CSV (eventNames)...");

    if (
      await verifyUser(
        idNumber,
        String(parsed.first),
        getConfig(subdomain, "ROLL_FILE_ID"),
        getConfig(subdomain, "ROLL_GID"),
        getConfig(subdomain, "GOOGLE_CREDENTIALS")
      )
    ) {
      return JSON.stringify(
        (await getCSV(
          getConfig(subdomain, "SERVICE_FILE_ID"),
          getConfig(subdomain, "SERVICE_GID"),
          getConfig(subdomain, "GOOGLE_CREDENTIALS")
        ))[0]
      );
    }
    return "Invalid login";
  } else if (parsed.get === "meetingDates") {
    console.log("- Downloading and interpreting CSV (meetingDates)...");

    if (
      await verifyUser(
        idNumber,
        String(parsed.first),
        getConfig(subdomain, "ROLL_FILE_ID"),
        getConfig(subdomain, "ROLL_GID"),
        getConfig(subdomain, "GOOGLE_CREDENTIALS")
      )
    ) {
      return JSON.stringify(
        (await getCSV(
          getConfig(subdomain, "ROLL_FILE_ID"),
          getConfig(subdomain, "ROLL_GID"),
          getConfig(subdomain, "GOOGLE_CREDENTIALS")
        ))[0]
      );
    }
    return "Invalid login";
  } else if (parsed.get === "login") {
    console.log("Checking user information...");

    return JSON.stringify(
      await verifyUser(
        parseInt(String(idNumber)),
        String(parsed.first),
        getConfig(subdomain, "ROLL_FILE_ID"),
        getConfig(subdomain, "ROLL_GID"),
        getConfig(subdomain, "GOOGLE_CREDENTIALS")
      )
    );
  }

  return "";
}
