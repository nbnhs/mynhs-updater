import authorize from "./auth";
import { parse } from "papaparse";
import { OAuth2Client } from "google-auth-library";
import fetch from "node-fetch";
import { GCredentials } from "../SubdomainConfig";

/**
 * Turns a CSV (Comma-separated values) file into a 2-dimensional array with the Papaparse library.
 * @param s The CSV to interpret into a string array.
 */
async function csv2arr(s: string): Promise<string[][]> {
  return new Promise((resolve, reject) => {
    parse(s, {
      complete: results => {
        console.log("- - - Parsed a CSV string!");
        resolve(results.data);
      },
      error: error => {
        console.log("- - - Failed to parse a csv string");
        reject({
          status: error,
          statusText: error.message
        });
      }
    });
  });
}

/**
 * Downloads the Google Sheet at the given ID as a 2-dimensional string array.
 * @param ofFileID The file ID of the Google sheet (e.g. in https://docs.google.com/spreadsheets/d/1dvKM0imVlxN_Ps7lIrcB3F64kj3PmF7H6CQoyEURvKc the file ID is 1dvKM0imVlxN_Ps7lIrcB3F64kj3PmF7H6CQoyEURvKc).
 * @param sheetID The ID of the individual sheet within the Google Sheet (e.g. in https://docs.google.com/spreadsheets/d/1qI7VVgwlIUholck1_jFAGfDaiMVOiIqO1jDhSpzUEhk/edit#gid=1802032102, the sheetID is 1802032102).
 * @param credentials The `GOOGLE_CREDENTIALS` object in the sheets config.
 */
export default async function getCSV(
  ofFileID: string,
  sheetID: string,
  credentials: GCredentials
): Promise<string[][]> {
  console.log(`- - - Processing file ${ofFileID}`);

  const creds: OAuth2Client = await authorize(
    credentials.wrapper,
    credentials.token
  );
  const access = (await creds.getAccessToken()).token;

  const res = await fetch(
    `https://www.googleapis.com/drive/v3/files/${ofFileID}/export?gid=${sheetID}&mimeType=text%2Fcsv`,
    {
      headers: {
        Authorization: `Bearer ${access}`,
        Accept: "application/json"
      }
    }
  );

  console.log(`Got response ${res}`);
  return await csv2arr(await res.text());
}
