import getCSV from "./getCSV";
import { GCredentials } from "../SubdomainConfig";

/**
 * Behaves like grep (UNIX tool for searching by regex) in that this function will find the value toGet in a csv, and return the line which contains it.
 * @param drive_file_id the ID of the Google Drive Sheets file to parse as a CSV.
 * @param sheet_gid the sheet ID of the individual sheet in the Google Sheets file to parse as CSV.
 * @param toGet value to search for. Must be able to be converted to a string.
 * @param credentials The `GOOGLE_CREDENTIALS` object in the sheets config.
 * @returns the line which contains toGet.
 */
export async function grepCSV(
  drive_file_id: string,
  sheet_gid: string,
  toGet: any,
  credentials: GCredentials
): Promise<string[]> {
  const parsed = await getCSV(drive_file_id, sheet_gid, credentials);
  console.log("- - Parsing obtained CSV file...");

  return parsed.find((o: string[]) => o[0] === String(toGet));
}

/**
 * Verifies a user based on provided information.
 * @param id ID card number of the user.
 * @param first the user's first name.
 * @param credentials The `GOOGLE_CREDENTIALS` object in the sheets config.
 * @returns true if the user exists and their name in the database equals the provided first name; false otherwise
 */
export async function verifyUser(
  id: number,
  first: string,
  roll_file_id: string,
  roll_sheet_gid: string,
  credentials: GCredentials
): Promise<boolean> {
  const user = await grepCSV(roll_file_id, roll_sheet_gid, id, credentials);

  console.log("- Got parsed CSV file. Checking if first is valid...");

  try {
    return user[2].toLowerCase() === first.toLowerCase();
  } catch (error) {
    console.log(error);
    return false;
  }
}
