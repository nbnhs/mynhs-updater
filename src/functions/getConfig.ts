import * as fs from "fs";

type ConfigOption =
  | "GOOGLE_CREDENTIALS"
  | "ROLL_FILE_ID"
  | "ROLL_GID"
  | "SERVICE_FILE_ID"
  | "SERVICE_GID";

/**
 * Retrieves a configuration option for the provided subdomain (or if needed the `default`).
 * @param subdomain Subdomain to retrieve configuration from.
 * @param option Option to retrieve.
 */
export default function getConfig(
  subdomain: string,
  option: ConfigOption
): any {
  const config = JSON.parse(fs.readFileSync("sheets_config.json", "utf-8"));

  return config[subdomain][option] || config["default"][option];
}
