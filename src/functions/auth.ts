import { google } from "googleapis";
import { GCredentialWrapper, GToken } from "../SubdomainConfig";

/**
 * Creates a Google-approved credentials object which can be used to directly access the API (within scope).
 * @param credential_wrapper The `wrapper` object under `GOOGLE_CREDENTIALS` in the sheets config.
 * @param token The `token` object under `GOOGLE_CREDENTIALS` in the sheets config
 */
export default function authorize(
  credential_wrapper: GCredentialWrapper,
  token: GToken
): Promise<any> {
  return new Promise((resolve, _) => {
    console.log("- - Parsing creds file...");
    const {
      client_secret,
      client_id,
      redirect_uris
    } = credential_wrapper.installed;
    const oAuth2Client = new google.auth.OAuth2(
      client_id,
      client_secret,
      redirect_uris[0]
    );
    console.log("- - Parsed.");

    console.log("- - Parsing tokens...");
    oAuth2Client.setCredentials(token);
    console.log("- - Parsed.");
    resolve(oAuth2Client);
  });
}
