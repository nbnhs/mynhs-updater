import * as https from "https";
import * as fs from "fs";
import switchboard from "./switchboard";

https
  .createServer(
    {
      key: fs.readFileSync("../privkey.pem"), // private key used for SSL certification
      cert: fs.readFileSync("../fullchain.pem") // SSL certificate
    },
    (request, response) => {
      response.writeHead(200, {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept"
      });

      if (request.method === "GET") {
        switchboard(request).then(val => {
          response.end(val);
        });
      }
    }
  )
  .listen(443);
