export interface GCredentialWrapper {
    installed: GCredentialMetadata
}

export interface GCredentialMetadata {
  client_id: string;
  project_id: string;
  auth_uri: string;
  token_uri: string;
  auth_provider_x509_cert_url: string;
  client_secret: string;
  redirect_uris: string[];
}

export interface GToken {
  access_token: string;
  refresh_token: string;
  scope: string;
  token_type: string;
  expiry_date: number;
}

export interface GCredentials {
  wrapper: GCredentialWrapper
  token: GToken
}

export interface SubdomainConfig {
  GOOGLE_CREDENTIALS: GCredentials
  ROLL_FILE_ID: string;
  ROLL_GID: string;
  SERVICE_FILE_ID: string;
  SERVICE_GID: string;
}
