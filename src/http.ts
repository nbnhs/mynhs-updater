import * as http from "http";
import switchboard from "./switchboard";

http
  .createServer((request, response) => {
    response.writeHead(200, {
      "Content-Type": "text/plain",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers":
        "Origin, X-Requested-With, Content-Type, Accept"
    });

    if (request.method === "GET") {
      switchboard(request).then(val => {
        response.end(val);
      });
    }
  })
  .listen(process.env.PORT);
